<?php

use Gregwar\Image\Image;

$app->match('/', function() use ($app) {
    return $app['twig']->render('home.html.twig');
})->bind('home');

//All the books
$app->match('/books', function() use ($app) {
    return $app['twig']->render('books.html.twig', array(
        'books' => $app['model']->getBooks()
    ));
})->bind('books');

//One book with exemplar
$app->match('/books/{id}', function($id) use ($app) {
    return $app['twig']->render('book.html.twig', array(
        'book' => $app['model']->getBook($id),
        'nb_exemplaires' => $app['model']->getExemplars($id),
        'exemplairesV' => $app['model']->getExemplarsValid($id),
        'exemplairesI' => $app['model']->getExemplarsInvalid($id)
    ));
})->bind('book');

//Take an exemplar
$app->match('books/{id}/takeExemplar', function($id) use ($app){
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (($_POST['nom'])!=null && ($_POST['dateD'])!=null && ($_POST['dateF'])!=null) {
            $app['model']->addEmprunt($_POST['nom'], $_POST['dateD'], $_POST['dateF'], $id);
        }
    }

    return $app['twig']->render('takeExemplar.html.twig', array(
        'id' => $app['model']->getBook($id)
    ));

})->bind('takeExemplar');

//Return an exemplar
$app->match('books/ReturnExemplar({id})', function($id) use ($app){
    return $app['twig']->render('ReturnExemplar.html.twig', array(
        'id' => $app['model']->getBook($id)
    ));

})->bind('returnExemplar');

//Admin session
$app->match('/admin', function() use ($app) {
    $request = $app['request'];
    $success = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('login') && $post->has('password') &&
            array($post->get('login'), $post->get('password')) == $app['config']['admin']) {
            $app['session']->set('admin', true);
            $success = true;
        }
    }
    return $app['twig']->render('admin.html.twig', array(
        'success' => $success
    ));
})->bind('admin');

//Logout
$app->match('/logout', function() use ($app) {
    $app['session']->remove('admin');
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('logout');

//Add a book
$app->match('/addBook', function() use ($app) {
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }

    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('title') && $post->has('author') && $post->has('synopsis') &&
            $post->has('copies')) {
            $files = $request->files;
            $image = '';

            // Resizing image
            if ($files->has('image') && $files->get('image')) {
                $image = sha1(mt_rand().time());
                Image::open($files->get('image')->getPathName())
                    ->resize(240, 300)
                    ->save('uploads/'.$image.'.jpg');
                Image::open($files->get('image')->getPathName())
                    ->resize(120, 150)
                    ->save('uploads/'.$image.'_small.jpg');
            }

            // Saving the book to database
            $app['model']->insertBook($post->get('title'), $post->get('author'), $post->get('synopsis'),
                $image, (int)$post->get('copies'));
        }
    }

    return $app['twig']->render('addBook.html.twig');
})->bind('addBook');