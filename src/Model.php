<?php

class Model
{
    protected $pdo;

    public function __construct(array $config)
    {
        try {
            if ($config['engine'] == 'mysql') {
                $this->pdo = new \PDO(
                    'mysql:dbname='.$config['database'].';host='.$config['host'],
                    $config['user'],
                    $config['password']
                );
                $this->pdo->exec('SET CHARSET UTF8');
            } else {
                $this->pdo = new \PDO(
                    'sqlite:'.$config['file']
                );
            }
        } catch (\PDOException $error) {
            throw new ModelException('Unable to connect to database');
        }
    }

    /**
     * Put out one precise result
     */
    protected function fetchOne(\PDOStatement $query)
    {
        if ($query->rowCount() != 1) {
            return false;
        } else {
            return $query->fetch();
        }
    }

    /**
     * Tries to execute a statement, throw an explicit exception on failure
     */
    protected function execute(\PDOStatement $query, array $variables = array())
    {
        if (!$query->execute($variables)) {
            $errors = $query->errorInfo();
            throw new ModelException($errors[2]);
        }

        return $query;
    }

    /**
     * Inserting a book in the database
     */
    public function insertBook($title, $author, $synopsis, $image, $copies)
    {
            $query = $this->pdo->prepare('INSERT INTO livres (titre, auteur, synopsis, image)
                VALUES (?, ?, ?, ?)');
            $this->execute($query, array($title, $author, $synopsis, $image));
            for($i=0; $i<$copies; $i++){
                $query2 = $this->pdo->prepare('INSERT INTO exemplaires (book_id) VALUES (?)');
                $id = getIdBook($title);
                $this->execute($query, array($id));
            }
            return $this->redirect("home");
    }

    /**
     * Getting all the books
     */
    public function getBooks()
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres');

        $this->execute($query);

        return $query->fetchAll();
    }

    /**
     * Getting all the exemplars with book id
     */
    public function getExemplars($id){

        $query = $this->pdo->prepare('SELECT exemplaires.* FROM exemplaires');
        $this->execute($query);
        return $query->fetchAll();
    }

    /**
    * Select all the exemplar valids of one book with its id
    */
    public function getExemplarsValid($id){
        $sql = 
              'SELECT exemplaires.* FROM exemplaires '.
              'INNER JOIN emprunts on exemplaires.id = emprunts.exemplaire '.
              'WHERE exemplaires.book_id = ? AND emprunts.fini = 1 '.
              'UNION SELECT exemplaires.* FROM exemplaires '.
              'WHERE exemplaires.book_id = ? AND exemplaires.id NOT IN('.
              'SELECT emprunts.exemplaire FROM emprunts)'
               ;

        $query = $this->pdo->prepare($sql);
        $query->execute(array($id, $id));

        return $query->fetchAll();
    }

    public function getExemplarsInvalid($id){
        $sql = 
              'SELECT * FROM exemplaires ' .
              'INNER JOIN emprunts ON exemplaires.id = emprunts.exemplaire '.
              'WHERE exemplaires.book_id = ? AND emprunts.fini = 0' ;

        $query = $this->pdo->prepare($sql);
        $query->execute(array($id));

        return $query->fetchAll();
    }

    /**
    * Adding an emprunt
    */
    public function addEmprunt($nom, $dateD, $dateF, $id){
        $query = $this->pdo->prepare('INSERT INTO emprunts (personne, exemplaire, debut, fin, fini) '.
                'VALUES (?, ?, ?, ?, false)');
            $this->execute($query, array($nom, $id, $dateD, $dateF));
    }

    /**
    * Select all the book for finding one
    */
    protected function getBookSQL()
    {
        return
            'SELECT livres.titre, livres.id, livres.image, livres.auteur, livres.synopsis FROM livres ';
    }

    /**
    *  Getting one book with id
    */
    public function getBook($id)
    {
        $sql = 
            $this->getBookSQL() . 
            'WHERE livres.id = ?' ;

        $query = $this->pdo->prepare($sql);
        $query->execute(array($id));

        return $query->fetchAll();
    }

    /**
    *  Return one exemplar to valid
    */
    public function ReturnExemplar($id){
        $sql = 
              'UPDATE emprunts ' .
              'SET fini = 1'.
              'WHERE livres.id = ?' ;
        $query = $this->pdo->prepare($sql);
        $query->execute(array($id));
    }
}