# Détails du projet - Merci de me lire ! #

* Le projet marche mais les emprunts se font d'une manière étrange et les affichages des emprunts ne se mettent pas à jour sur les fiches des livres sauf si je modifie la base de données.

* Par contre, j'ai remarqué des bugs entre les différents navigateurs utilisés. Par exemple, sous Chromium, comme demandé des datePicker s'affiche (pour l'emprunt), mais pas sous Iceweasel. Sous Chromium de plus, la date de départ n'est pas initialisée alors que sous Iceweasel oui.

* J'ai essayé de respecter la consigne d'un commit par question, mais j'ai du apporter des modifications / corrections au fur est à mesure...